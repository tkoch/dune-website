+++
title = "Dune Developer Meeting 2018 in Stuttgart"
+++

Date
----

The developer meeting is held on November 7th and 8th, 2018 in Stuttgart.

The meeting is co-located with the [Dune User Meeting 2018](https://dune-project.org/community/meetings/2018-11-usermeeting/).

Participants
-------------

- Andreas Dedner
- Christoph Grüninger (only Wednesday, 7th Nov)
- Christian Engwer
- Robert Klöfkorn
- Oliver Sander

Proposed topics
---------------

Please use the edit function to add your topics or write an email to [the organizers](https://www.ians.uni-stuttgart.de/institute/news/events/Dune-User-and-Developer-Meeting-2018/).


## General

* RK: Stability of the gitlab infrastructure. How can we help to improve it? 
* CGü: I'd like to see an hour of cleaning up. Together we scan (one or two of)
  - merge requests
  - bugs
  - the remaining open FlySpray tasks
  - Git branches
  
  and decide which can be closed / deleted right away and which to keep.

## dune-common

## dune-geometry

## dune-grid

* OS: ALUGrid has been implementing some improvements to the distributed grids
  interface.  Can we make some of the part of the official interface?  
  - RK: That would be nice and should not be much work or disturbance for other grid implementations.
  - CGü: Do you have concret suggestions what to include?

* RK: Should we add a grid implementation providing polyhedral grids?
  - CE: I'm in favor of adding a wide range of different grids to the core to improve the first shot user experience.

## dune-localfunctions

## dune-istl

* RK: What is needed to make dune-istl a more self contained LA solver package?
* CE: I'd like to finish our long standing effort of fully dynamic solver interfaces and a solver factory. There are a few smaller questions where I'd like to have some feedback from the core developers.
* OS: The fact that FieldVector<X,1> and FieldMatrix<X,1,1> get special treatment
      is the source of various inconsistencies and general unhappiness.  Can we review that decision?
      There are two competing proposals:
  - Make nesting recursion end at regular number types, rather than at FieldVector/FieldMatrix.
    For example, allow `BCRSMatrix<double>` and make it do what people would expect.
  - Introduce a dedicated new type `Field<T>` that should wrap number types T and that
    should be used to end recursions.
    This would mean that `BCRSMatrix<FieldMatrix<double,1,1> >` should be replaced by
    `BCRSMatrix<Field<double> >`.

  The interesting question: Can we do that in a backward-compatible way?     

## Discretization modules (FEM, PDELab, ...)

* CE: can we agree on a new common discrete function interface based on dune-functions? Current state is
  - dune-functions defines interfaces for functions and global bases.
  - a couple of people are now working directly with dune-functions, without any other discretization module.
  - PDELab supports the new function interface together with "old-style" PDELab gridfunctionspaces
  - FUFEM is now mostly based on dune-functions
* CGrä: Some comments on CE's proposal above:
  - Notice that dune-functions `localFunction(f)` interface is already supported in the `VTKWriter`
  - When discussing the proposal please mention the subtleties of
    `localFunction(derivative(f))` vs `derivative(localFunction(f))` to avoid
    later surprises.

## python support

* CE: How do we envision future code development regarding python?


# Protocol

## User Meeting Input

- Parallel grid construction
- Data I/O (HDF5 / XDMF)
- Periodic Boundaries
- Binary operators for dense linear algebra
- Dune packaging for spack https://github.com/spack/spack
- News entries on homepage for applications to build application showcase


## Agenda

Suggestion by Oliver: working groups

- Documentation / website
- Build system: WG (Steffen, Markus, Dominic, Jö, Linus)
- Discuss new Dune paper: Thursday
- Development Process (handling of MRs etc.)
- Bindings for other programming languages
- Infrastructure
- MPI rework
- New way of handling scalar LA entities (`FieldVector<T,1>`, `FieldMatrix<T,1,1>`) (Jö, Oliver, Claus)
- Minimal versions of toolchain components
- Dynamic selection of solver components in ISTL
- I/O: WG (Steffen, Timo, Markus)
- dune-localfunctions interface: WG (Oliver, Andreas, Christian)
- twists and grid topology: WG (Marcel, Christian, Claus, Robert)
- dune-functions as a foundation for discretization modules
- parallel grid interface including parallel grid construction: WG (Robert, Markus, Linus, Oliver, Andreas, Christian, Nils)


## Build System

Input

- Oliver: Alias `dunecontrol cmake` for `dunecontrol configure`
- Build without dunecontrol
  - "Master CMake project" with subprojects for each module
  - conan.io package manager


## Website

- Add a rotating image gallery (JavaScript anyone?)
- Grid documentation: Make it easier to find list of [grid features](https://www.dune-project.org/about/gridmanager-features/)


## Miscellaneous

- What does `Entity::regular()` do?
  - Nobody knows whether it's needed
  - Remove it
- `GeometryType::GeometryType(int dim)` and `GeometryType::GeometryType(unsigned int dim)`
  - only work for vertices and lines
  - remove
- Improve deprecation macros to encode version
- `MPIHelper::instance()` why not const ref?


## Release Procedure

- Time-Based releases
  - Requires automatic testing

## I/O

- Do we want to support generic backup / restore using `GridFactory` or something similar?
- Use Python bridge for backup / restore
- Improve backup / restore documentation
