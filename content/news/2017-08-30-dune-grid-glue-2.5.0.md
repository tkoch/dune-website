+++
date = "2017-08-30"
title = "dune-grid-glue 2.5.0 released"
+++

A new version of _dune-grid-glue_, a module to provide various forms of couplings between different grids, has been released.
The new _dune-grid-glue_ 2.5.0 release is compatible with DUNE 2.5 and includes various bugfixes; there are no major new features in this release.

The module can be downloaded from the [dune-grid-glue page].

  [dune-grid-glue page]: https://www.dune-project.org/modules/dune-grid-glue
