+++
date = "2018-02-10"
title = "Dune 2.6.0rc2 Released"
+++

The second release candidate for the upcoming 2.6 release is now available.
You can [download the tarballs](/releases/2.6.0rc2), checkout the `v2.6.0rc2`
tag via Git - there is no prebuilt packages from Debian experimental yet.
Please go and test, and report any problems that you encounter.

Included in the release candidate are the [core modules][] (dune-common,
dune-geometry, dune-grid, dune-istl, dune-localfunctions).
Release candidates for dune-alugrid and dune-python are also included.

Please refer to the [recent changes] page for an overview of what has
changed in the new release -
please let us know if something is missing from there.

  [core modules]: https://gitlab.dune-project.org/core/
  [recent changes]: /dev/recent-changes

