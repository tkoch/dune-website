+++
date = "2007-03-28"
title = "Dune 1.0beta4 available"
+++

Today we released the fourth and hopefully final beta release for the three core modules `dune-common`, `dune-grid`, `dune-istl` and the `dune-grid-howto`.

Go to the *download page*, grab the packages, test them!
