+++
date = "2007-08-17"
title = "Dune 1.0beta6 available"
+++

After several bugs fixes, we released today the sixth and hopefully final beta release for the three core modules `dune-common`, `dune-grid`, `dune-istl` and the `dune-grid-howto`. We fixed several bugs concernig the documentation, beside these the code seems to be fairly mature.

Go to the *download page*, grab the packages, test them!
